# Hello, Rust Macros!

A slide deck explaining macros with lots of examples

Trying out [remark.js](https://github.com/gnab/remark) to create web-based
slides in Markdown.

Lots of notes on the [wiki](https://gitlab.com/rcousineau/hello-rust-macros/wikis/Home)!
