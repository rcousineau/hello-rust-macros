
class: center, middle

[//]: # (1)

# `macros_rule!`

---

[//]: # (2)

# Introduction

When I first started preparing for this presentation, I knew:

* macros exist
--

* they have an... interesting sytax
--

* there are a couple different types

--

If that's where you're at right now, you're in the perfect place!

???

My goal with this presentation is to hopefully make macros a little less
intimidating for you, and to add another tool to your toolbelt.

---

[//]: # (3)

# Outline

1. **What is a macro?**
--

2. **Write a declarative *`macro_rules!`* macro**
--

3. **Write a few procedural "proc" macros**
--

4. **Dissect some other interesting macros**

???

All of the examples will be available either as Rust Playground links or
repositories.

I'll share a link to this repository's Wiki, which is a collection of
information about macros including all of those examples.

---

[//]: # (4)

# What Is A Macro?

I will let [The Book](https://doc.rust-lang.org/book/ch19-06-macros.html)
answer this one.

> *Macros are a way of writing code that writes other code.*
> *Macros expand to produce more code than the code you’ve written manually.*

--

When should one reach for macros?

* macros, unlike functions, can take a variable number of parameters
--

* macros are expanded before the compiler interprets the meaning of the code

---

[//]: # (5)

# A Familiar Macro

You have no doubt seen **`println!`**.

```rs
println!();
// <blank line>
```
--

```rs
println!("Hello, world!");
// "Hello, world!"
```
--

```rs
let name = "PDX Rust";
println!("Hello, {}!", name);
// "Hello, PDX Rust!"
```

---

[//]: # (6)

# A Familiar Macro

But why is it a macro? Can it just be a function?

```rs
fn my_println(text: &str) {
    let line = format!("{}\n", text);
    simple_print(line.as_bytes());
}
```

(here is a helper to put some bytes on the screen)

```rs
fn simple_print(text: &[u8]) {
    io::stdout().write(text).expect("failed to write to stdout");
}
```

---

[//]: # (7)

# A Println Function

### Let's try it out!

```rs
my_println("Hello, world!");
// "Hello, world!"
```

No problem!

---

[//]: # (8)

# A Println Function

### Let's try it out!

```rs
my_println();
// error[E0061]: this function takes 1 parameter but 0 parameters were supplied
let name = "PDX Rust";
my_println("Hello, {}!", name);
// error[E0061]: this function takes 1 parameter but 2 parameters were supplied
```

Uh oh. It fails if you pass **zero** or **more than one** argument.

Rust functions cannot (usually) take a variable number of arguments.

Let's try to write a macro!

???

People often run into this "hurdle" of Rust not having function overloading or
default arguments.

A common solution is to use **`Option`** in function parameter types.

For interop with C varargs functions, there is the **`va_list`** crate.

---

[//]: # (9)

# My First Declarative Macro

We can define a macro with **`macro_rules!`** like this:

```rs
macro_rules! my_println {}
```

???

Feel free to follow along if you'd like!

At the end of this guide, there will be a link to the Rust Playground will a
fully functioning version of this macro to play with.

---

[//]: # (10)

# My First Declarative Macro

This style of macro is defined using pattern matching:

```rs
macro_rules! my_println {
    (
*       <pattern>
    ) => {
*       <result>
    };
}
```

---

[//]: # (11)

# My First Declarative Macro

A repeat in the pattern can be written with **`$( ),*`**. The **`,`** means
that we expect the repeated pattern to be comma-separated.

```rs
macro_rules! my_println {
    (
*       $( <pattern> ),*
    ) => {
        <result>
    };
}
```
--

Recall that one of our use cases involves the following patterns being passed:
* **`"Hello, {}!"`**
* **`name`**

In the expression:

```rs
my_println!("Hello, {}!", name);
```

---

[//]: # (12)

# My First Declarative Macro

Here **`$x:expr`** captures an **expression** into the variable `x`.

```rs
macro_rules! my_println {
    (
        $(
*           $x:expr
        ),*
    ) => {
        <result>
    };
}
```

???

Refer to the Little Book of Rust Macros to see other things that can be
captured:
https://danielkeep.github.io/tlborm/book/mbe-macro-rules.html#captures

> These captures leverage the Rust compiler's parser, ensuring that they are
> always "correct". An **`expr`** capture will always capture a complete, valid
> expression for the version of Rust being compiled.

---

[//]: # (13)

# My First Declarative Macro

We'll use our **`simple_print`** helper from earlier.

```rs
macro_rules! my_println {
    ( $( $x:expr ),* ) => {
        {
*           let mut s = unimplemented!();
*           // the "ln" part of println
*           s.push_str("\n");
*           simple_print(s.as_bytes());
        }
    };
}
```

???

`unimplemented!()` can be used anywhere and causes a panic if it gets executed

--

What if we tried this?

```rs
let mut s = String::from($x);
```

```
error: variable 'x' is still repeating at this depth

             let mut s = String::from($x);
                                      ^^
```

---

[//]: # (14)

# My First Declarative Macro

Remember that **`$($x:expr),*`** means that we're matching on zero or more
comma-separated values as input to this macro. When referencing **`$x`** we'll
need to use the syntax **`$($x),*`** if we want to output those values
comma-separated.

We'll cheat a tiny bit and call another macro **`format!`** that takes the same
arguments as **`println!`**.

```rs
macro_rules! my_println {
    ( $( $x:expr ),* ) => {
        {
            let mut s = format!($($x),*);
            s.push_str("\n");
            simple_print(s.as_bytes());
        }
    };
}
```

---

[//]: # (15)

# My First Declarative Macro

### Let's try it out!

```rs
my_println!("Hello, world!");
// "Hello, world!"
```
--

```rs
my_println!("Hello, {}!", "PDX Rust");
// "Hello, PDX Rust!"
```

Our macro can now support one **OR** two argument(s)!

---

[//]: # (16)

# My First Declarative Macro

### Let's try it out!

```rs
my_println!();
// error: requires at least a format string argument
```
--

But it seems there's a bug!

The **`format!`** macro, which we're calling, doesn't allow passing zero
arguments.

---

[//]: # (17)

# My First Declarative Macro

```rs
macro_rules! my_println {
*   // could do `simple_print(b"\n")` to achieve parity with `println!`
*   () => { simple_print(b"<blank line>\n") };
    ( $( $x:expr ),* ) => {
        <snip>
    };
}
```

???

Earlier I mentioned that we define declarative macros using pattern matching.

By adding a new empty pattern, we can customize the behavior of our macro for
calls with no arguments.

We can do pretty much anything here, but we'll use our **`simple_print`**
helper to print a message so that it's obvious when this code is executed.

---

[//]: # (18)

# My First Declarative Macro

### Let's try it out!

```rs
my_println!();
// <blank line>
```

---

[//]: # (19)

# My First Declarative Macro

To recap:

```rs
fn simple_print(text: &[u8]) {
    io::stdout().write(text).expect("failed to write to stdout");
}

macro_rules! my_println {
    () => { simple_print(b"<blank line>\n") };
    ( $( $x:expr ),* ) => {
        {
            let mut s = format!($($x),*);
            s.push_str("\n");
            simple_print(s.as_bytes());
        }
    };
}
```

[Playground](https://play.rust-lang.org/?gist=9146ccb27beb9b27f34feea7cefe1236)

---

class: center, middle

[//]: # (20)

# Intermission

> *Strive not to be a success, but rather to be of value.*

> *–Albert Einstein*

???

And next we will write a macro which has **no** value.

---

[//]: # (21)

# Procedural Macros

When should one reach for procedural macros?

* attributes on enums, structs, functions, fields, etc.
--

* **`derive`** a trait implementation
--

* function-like macros - there are limitations on what patterns can be matched
  via **`macro_rules!`** (see
  [captures](https://danielkeep.github.io/tlborm/book/mbe-macro-rules.html#captures))

???

We're going to explore writing all three kinds!

---

[//]: # (22)

# My First Procedural Macro

Next we will write a very simple procedural macro, of the **`derive`** variety.

It will allow us to annotate a struct or enum with **`#[derive(Identity)]`** to
get a derived implementation of an **`identity`** method that simply prints the
name of the struct or enum that we annotated.

```rs
#[derive(Identity)]
struct Foo;

fn main() {
    Foo::identity(); // prints "Identity: Foo"
}
```

???

This is not a particularly useful macro, but we'll use it to explore the
machinery of procedural macros.

---

[//]: # (23)

# My First Procedural Macro

Procedural macros are required to be in their own separate crate and the
Cargo.toml file must have this option set:

```toml
[lib]
proc-macro = true
```

So we will be building two crates: a library crate with our trait, and the
macro crate.

Cargo has a convenient feature we can leverage called
[Workspaces](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html) for
grouping crates together in a project.

---

[//]: # (24)

# My First Procedural Macro

The layout of the files will end up something like this:

```
project/
  Cargo.toml
  my-trait/
    src/
      lib.rs
    Cargo.toml
  my-trait-derive/
    src/
      lib.rs
    Cargo.toml
```
--

The top-level Cargo.toml will define the workspace:

```toml
[workspace]
members = [
    "my-trait",
    "my-trait-derive"
]
```

---

[//]: # (25)

# My First Procedural Macro

Let's create a crate with our trait in it. We'll call it **`identity`**.

```sh
cargo new --lib identity
```
--

We'll put our trait in src/lib.rs and then we're done.

```rs
pub trait Identity {
    fn identity();
}
```

---

[//]: # (26)

# My First Procedural Macro

Next is our macro crate.

```sh
cargo new --lib identity-derive
```
--

To declare a **`derive`** procedural macro, we'll put an annotated function in
src/lib.rs:

```rs
extern crate proc_macro;

use proc_macro::TokenStream;

#[proc_macro_derive(Identity)]
*pub fn identity_derive(input: TokenStream) -> TokenStream {}
```
--

Using the **`proc_macro_derive`** attribute and specifying **`Identity`** means
that this macro can be used to derive an **`Identity`** implementation for a
**`struct`** or an **`enum`** with **`#[derive(Indentity)]`** (just like you
would for any of the standard
[derivable traits](https://doc.rust-lang.org/stable/book/appendix-03-derivable-traits.html)).

---

[//]: # (27)

# My First Procedural Macro

```rs
#[proc_macro_derive(Identity)]
pub fn identity_derive(
*   input: TokenStream
) -> TokenStream {}
```

The **`proc_macro::TokenStream`** that is passed to all procedural macros is:

> *an abstract stream of tokens, or, more specifically, a sequence of token trees*

--

For the case of **`derive`** macros, the **`input`** is the entire **`struct`**
or **`enum`** with the attribute on it.

```rs
#[derive(Identity)]
*struct Foo;
```

---

[//]: # (28)

# My First Procedural Macro

```rs
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
*   let ast = parse_macro_input!(input as DeriveInput);
}
```

The standard approach to writing procedural macros starts with constructing an
abstract syntax tree (AST) from this stream using the **`syn`** crate.

The **`syn`** crate provides a very handy **`DeriveInput`** type that can be
used for parsing a derived **`struct`** or **`enum`**.

---

[//]: # (29)

# My First Procedural Macro

Our derived **`Identity`** implementation is going to provide a method that
prints the name of the **`struct`** or **`enum`** it is derived for. So we need
a way to extract the name from the parsed input.

```rs
#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
*   let name = &ast.ident;
}
```

The **`syn::DeriveInput`** type makes the name available via **`ident`**.

---

[//]: # (30)

# My First Procedural Macro

In addition to the **`syn`** crate for parsing a **`TokenStream`** in a proc
macro, there is the **`quote`** crate for constructing a new **`TokenStream`**.

```rs
use quote;

#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
*   let gen = quote! {
*       impl Identity for #name {
*           fn identity() {}
*       }
*   };
}
```

The **`quote`** crate comes with a **`quote!`** macro that allows us to write
Rust code and (eventually) turn it into a **`TokenStream`**! The symbol
**`#name`** here is replaced with the value of the **`name`** variable.

---

[//]: # (31)

# My First Procedural Macro

Next we'll finish writing the **`identity`** function (which calls a couple
*more* macros!).

```rs
#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
    let gen = quote! {
        impl Identity for #name {
            fn identity() {
*               println!("Identity: {}", stringify!(#name));
            }
        }
    };
}
```

The **`stringify!`** macro turns the tokens it parses into a string, which will
occur *after* **`quote!`** has replaced **`#name`** with the actual name.

---

[//]: # (32)

# My First Procedural Macro

```rs
#[proc_macro_derive(Identity)]
pub fn identity_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
    let gen = quote! {
        impl Identity for #name {
            fn identity() {
                println!("Identity: {}", stringify!(#name));
            }
        }
    };
*   TokenStream::from(gen)
}
```

Lastly we just need to convert the result to a **`TokenStream`**.

---

[//]: # (33)

# My First Procedural Macro

### Let's try it out!

```rs
use identity::Identity;
use identity_derive::Identity;

#[derive(Identity)]
struct Foo;
#[derive(Identity)]
struct Bar;
#[derive(Identity)]
enum Baz {}

Foo::identity();
// Identity: Foo
Bar::identity();
// Identity: Bar
Baz::identity();
// Identity: Baz
```

[Repo](https://gitlab.com/rcousineau/derive-identity-proc-macro)

---

class: center, middle

[//]: # (34)

# Intermission

> *There is only one way to avoid criticism: do nothing, say nothing, and be*
> *nothing.*

> *–Aristotle*

---

[//]: # (35)

# My Second Procedural Macro

Now let's write a different kind of procedural macro. We're going to create a
proc macro version of our `my_println!` macro from earlier.

```rs
#[proc_macro]
pub fn my_println(input: TokenStream) -> TokenStream {
```

We will plan to call **`parse_macro_input`** and pass it the **`input`**, but
first we'll need to define a type we can use to represent the parsed input.

---

[//]: # (36)

# My Second Procedural Macro

Since this isn't a derive macro, we can't use **`DeriveInput`**. Instead, we'll
define a type that represents this macro's input. This macro accpets zero or
more Rust expressions.

```rs
use syn::{
    punctuated::Punctuated,
    Expr, Token
};

struct MyPrintlnArgs {
    args: Punctuated<Expr, Token![,]>,
}
```

???

If you're not already, by this point you will start to become very intimately
familiar with the `syn` documentation.

---

[//]: # (37)

# My Second Procedural Macro

Next we need to implement the **`syn::parse::Parse`** trait for our new type.

```rs
use syn::{
    parse::{Parse, ParseStream},
    Result as ParseResult
};

impl Parse for MyPrintlnArgs {
    fn parse(input: ParseStream) -> ParseResult<Self> {
        let args = Punctuated::parse_terminated(input)?;
        Ok(MyPrintlnArgs { args })
    }
}
```

---

[//]: # (38)

# My Second Procedural Macro

```rs
use syn::parse_macro_input;

#[proc_macro]
pub fn my_println(input: TokenStream) -> TokenStream {
*   let parsed = parse_macro_input!(input as MyPrintlnArgs);
*   let args = parsed.args.into_iter();
```

---

[//]: # (39)

# My Second Procedural Macro

Now we have an instance of our new type, representing the Rust code that was
passed to our macro. Similar to our **`macro_rules!`** macro earlier, we will
now just expand the expressions that were passed. We'll use **`quote!`** again,
along with the repetition syntax: **`#(#args),*`**.

```rs
#[proc_macro]
pub fn my_println(input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as MyPrintlnArgs);
    let args = parsed.args.into_iter();

*   let gen = quote! {
*       {
*           fn simple_print(text: &[u8]) {
*               use std::io::*;
*               io::stdout().write(text).expect("failed to write to stdout");
*           }
*           simple_print(format!(#(#args),*).as_bytes());
*           simple_print(b"\n");
*       }
*   }
```

---

[//]: # (40)

# My Second Procedural Macro

An incredibly powerful feature of **`quote!`** is that you can reference
`quote`d Rust code for reuse.

```rs

*   let simple_print = quote! {
*       fn simple_print(text: &[u8]) {
*           use std::io::*;
*           std::io::stdout().write(text).expect("failed to write to stdout");
*       }
*   };

    let gen = quote! {
        {
*           #simple_print
            simple_print(format!(#(#args),*).as_bytes());
            simple_print(b"\n");
        }
    };
```

---

[//]: # (41)

# My Second Procedural Macro

### Let's try it out!

```rs
use my_println::my_println;

fn main() {
    my_println!();
    my_println!("Hello, world!");
    my_println!("Hello, {}!", "PDX Rust");
}
```

This unfortunately doesn't compile unless the ***user*** of this macro adds
**`#![feature(proc_macro_hygiene)]`** to their main.rs, which requires nightly.

```
error[E0658]: procedural macros cannot be expanded to statements
```

[Repo](https://gitlab.com/rcousineau/my-println-proc-macro)

---

[//]: # (42)

# My Second Procedural Macro

If only there were some "hack" so that these proc macros could work on stable
rust...

--

[https://github.com/dtolnay/proc-macro-hack](https://github.com/dtolnay/proc-macro-hack)

> *This crate implements an alternative type of procedural macro that can be*
> *invoked in statement or expression position.*

---

[//]: # (43)

# My Second Procedural Macro

Two crates:

* `my_println` - just exports the macro
* `my_println_impl` - defines the macro (`lib.proc-macro = true` in Cargo.toml)

--

Rename the existing **`my_println`** crate to **`my_println_impl`**, add the
**`proc-macro-hack`** crate to the dependencies in the Crate.toml file, and
change **`#[proc_macro]`** to **`#[proc_macro_hack]`**.

```rs
use proc_macro_hack::proc_macro_hack;

#[proc_macro_hack]
pub fn my_println(input: TokenStream) -> TokenStream {
```

--

Create new **`my_println`** crate.

```rs
use proc_macro_hack::proc_macro_hack;

#[proc_macro_hack]
pub use my_println_impl::my_println;
```

---

[//]: # (44)

# My Second Procedural Macro

### Let's try it out!

```rs
use my_println::my_println;

fn main() {
    my_println!();
    // <blank line>
    my_println!("Hello, world!");
    // "Hello, world!"
    my_println!("Hello, {}!", "PDX Rust");
    // "Hello, PDX Rust!"
}
```

[Repo](https://gitlab.com/rcousineau/my-println-proc-macro-hack)

---

class: center, middle

[//]: # (45)

# Intermission

> *You miss 100% of the shots you don’t take. –Wayne Gretzky*

> *–Michael Scott*

???

Hopefully that warmed you up for this next part, which is a little silly.

---

[//]: # (46)

# My Third Procedural Macro

We'll write one more procedural macro, this time of the **attribute** variety.
Attribute macros are very good for augmenting code. We'll write a macro that
completely replaces the contents of a function.

--

This macro is called `sec_through_obsc` because it implements "security"
through *obscurity*.

We'll *obscure* some of our code by serializing it to a base-64 string.

```sh
$ echo '(n * n) + 1' | base64
KG4gKiBuKSArIDEK
```

```rs
#[sec_through_obsc(KG4gKiBuKSArIDEK)]
fn square(n: i32) -> i32 {
    n * n
}

// square(2) -> 5
// square(3) -> 10
```

---

[//]: # (47)

# My Third Procedural Macro

We'll start out similarly to how we did with our other proc macros.

```rs
#[proc_macro_attribute]
pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
```

This time we have two **`TokenStream`** inputs:

* **`attr`** is what is passed in the attribute
  (**`#[sec_through_obsc(<attr>)]`**)
* **`item`** is the item with the attribute on it (a **`fn`** in our case)

---

[//]: # (48)

# My Third Procedural Macro

Next we parse the **`item`**, assuming it is a **`fn`**.

```rs
*use syn::{parse_macro_input, ItemFn};

#[proc_macro_attribute]
pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
*   let ast = parse_macro_input!(item as ItemFn);
```

--

If this macro isn't used on a **`fn`**, it will print a clear error, e.g.:

```
error: expected `fn`
 --> sample/src/main.rs:9:1
  |
9 | struct MyStruct;
  | ^^^^^^
```

---

[//]: # (49)

# My Third Procedural Macro

We'll come back to our parsed **`fn`**. First, let's deal with the *obscured*
token that was passed.

```rs
*use base64;
*use std::str::FromStr;

#[proc_macro_attribute]
pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(item as ItemFn);

*   let raw_body = match base64::decode(&attr.to_string()) {
*       Ok(decoded) => match String::from_utf8(decoded) {
*           Ok(s) => match TokenStream::from_str(&s) {
*               Ok(tokens) => tokens,
*               Err(e) => panic!("not valid tokens! {:?}", e),
*           },
*           Err(e) => panic!("not valid UTF-8! {}", e),
*       },
*       Err(e) => panic!("failed to decode! {}", e),
*   };
```

We're just creating a **`TokenStream`** from the string.

???

A helper function that returned `Result` would be really useful here...

---

[//]: # (50)

# My Third Procedural Macro

Now we're going to construct a **`syn::Expr`**.

```rs
*use proc_macro2;
*use syn::Expr;

pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(item as ItemFn);

    let raw_body = ...

*   let body = Expr::Verbatim(proc_macro2::TokenStream::from(raw_body));
```

???

This isn't *strictly* correct for all possible use cases...

---

[//]: # (51)

# My Third Procedural Macro

We can almost put it all together! We want to preserve the function signature,
so we'll output those parts of the parsed input.

```rs
*use quote::quote;

pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(item as ItemFn);
*   let attrs = ast.attrs.into_iter();
*   let vis = ast.vis;
*   let sig = ast.sig;
```

--

We'll use **`quote!`** again to generate our replacement function with the new
body (the **`Expr::Verbatim`** we just created).

```rs
    let gen = quote! {
        #(
            #attrs
        )*
        #vis #sig {
            #body
        }
    };
```

---

[//]: # (52)

# My Third Procedural Macro

Fun fact: if you **don't** create the **`Expr::Verbatim`**, then you end up
with a function that returns a ***string***!

```rs
fn double(n: i32) -> i32 {
    "\"(n * n) + 1\\n\""
}
```

(And reminder: **`cargo expand`** is your friend!)

---

[//]: # (53)

# My Third Procedural Macro

Lastly we just convert to a **`TokenStream`** like always.

```rs
#[proc_macro_attribute]
pub fn sec_through_obsc(attr: TokenStream, item: TokenStream) -> TokenStream {
    let ast = ...

    let raw_body = ...

    let body = Expr::Verbatim(TokenStream2::from(raw_body));

    let gen = quote! {
        #(
            #attrs
        )*
        #vis #sig {
            #body
        }
    };

*   TokenStream::from(gen)
}
```

---

[//]: # (54)

# My Third Procedural Macro

### Let's try it out!

```rs
use sec_through_obsc::sec_through_obsc;

#[sec_through_obsc(KG4gKiBuKSArIDEK)]
fn square(n: i32) -> i32 {
    n * n
}

fn main() {
    dbg!(square(2));
    // square(2) = 5
    dbg!(square(3));
    // square(3) = 10
}
```

[Repo](https://gitlab.com/rcousineau/sec-through-obsc-proc-macro)

---

class: center, middle

[//]: # (55)

# Intermission

> *Adapt what is useful, reject what is useless, and add what is specifically*
> *your own.*

> *–Bruce Lee*

---

[//]: # (56)

# Macros in the Wild

Now that we're a little more familiar with the macro building blocks, let's
practice reading some code and get a better understanding of some popular
macros.

---

[//]: # (57)

# Macros in the Wild

**`futures::join!`** (`#[proc_macro_hack]`)

> *Polls multiple futures simultaneously, returning a tuple of all results*
> *once complete.*

Example:

```rs
use futures::{executor::block_on, join};

async fn ab() -> (i32, i32) {
    let a = async { 1 };
    let b = async { 2 };

    join!(a, b)
}

fn main() {
    dbg!(block_on(ab()));
}
```

[Playground](https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=f529729f61b8c3d5dda6486afd5cc6f9)
 |
[`join!` source](https://github.com/rust-lang/futures-rs/blob/7153ba06eefae260665a5450fa3e95c6b57a74a6/futures-macro/src/lib.rs#L23)
 ->
[`join`](https://github.com/rust-lang/futures-rs/blob/7153ba06eefae260665a5450fa3e95c6b57a74a6/futures-macro/src/join.rs#L69)

???

Note that we need an executor in order for these futures to **do anything**.

---

[//]: # (58)

# Macros in the Wild

**`#[tokio::main]`** (`#[proc_macro_attribute]`)

> *Marks async function to be executed by selected runtime.*

Example: **DEMO**

[Source](https://github.com/tokio-rs/tokio/blob/3ecaa6d91cef271b4c079a2e28bc3270280bcee6/tokio-macros/src/lib.rs#L59)

---

[//]: # (59)

# Resources

### [Wiki](https://gitlab.com/rcousineau/hello-rust-macros/wikis/Home)

* macro learning resources
* more macro examples / macros in the wild

### Slides: https://rcousineau.gitlab.io/hello-rust-macros

### Src: https://gitlab.com/rcousineau/hello-rust-macros

slides (Markdown) built using remark.js

---

[//]: # (60)

class: center, middle

# fin

### Questions?
